# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Utils."""

from __future__ import absolute_import, print_function

import collections

from copy import deepcopy
from datetime import date
from simplejson import JSONEncoder
from functools import reduce

from .errors import WrongConfiguration
from .resources import ProxyResource


class ModelJSONEncoder(JSONEncoder):

    """JSON Encoder for model classes."""

    def default(self, obj):
        """Default encoder."""
        if isinstance(obj, ProxyResource) and obj._model:
            content = deepcopy(obj._model.__dict__)
            # remove '_additional_props' from the dictionary
            if '_additional_props' in content:
                del content['_additional_props']
            return content
        else:
            return obj


def adapt_data(data, done=None):
    """Adapt the data in a proper way to be able to save them."""
    done = done or []
    if isinstance(data, dict):
        for k, v in data.items():
            data[k] = adapt_data(v)
    elif isinstance(data, list):
        for k, v in enumerate(data):
            data[k] = adapt_data(v)
    elif isinstance(data, date):
        data = data.strftime("%Y-%m-%dT%H:%M:%SZ")

    return data


def dict_merge(mydict, merge_dict):
    """Recursive dict merge.

    :param mydict: original dict.
    :param merge_dict: mydict merged into mydict
    :return: None
    """
    for k, v in merge_dict.items():
        if (k in mydict and isinstance(mydict[k], dict)
                and isinstance(merge_dict[k], collections.Mapping)):
            dict_merge(mydict[k], merge_dict[k])
        else:
            mydict[k] = merge_dict[k]


class DotDict(dict):

    """A normal dictionary with the possibility to access with dot notation.

    E.g.

    .. code-block:: python

        fuu = DotDict({
            'a': {
                'b': 'c'
            },
            'd': 'e'
        })

        assert 'c' == fuu['a.b']
    """

    def __init__(self, d=None):
        """Init as a dictionary."""
        self._dict = d if d is not None else dict()
        super(DotDict, self).__init__()

    def __getitem__(self, key):
        """Get a value with dot notation."""
        value = self._dict
        try:
            for subkey in key.split('.'):
                value = value[subkey]
        except KeyError:
            raise WrongConfiguration(key)
        return value

    def __setitem__(self, key, value):
        """Set a value with dot notation."""
        dict_merge(self._dict, reduce(
            lambda res, cur: {cur: res},
            reversed(key.split(".")),
            value
        ))

    def get(self, key, default=None):
        """Override get."""
        try:
            return self._dict[key]
        except KeyError:
            return default

    def __iter__(self):
        """Iter."""
        return iter(self._dict)

    def keys(self):
        """Get keys."""
        return self._dict.keys()

    def values(self):
        """Get values."""
        return self._dict.values()

    def __str__(self):
        """Str."""
        return self._dict.__str__()

    def __repr__(self):
        """Repr."""
        return self._dict.__repr__()

    def items(self):
        """Items."""
        return self._dict.items()

    def popitem(self):
        """Pop item."""
        return self._dict.popitem()

    def copy(self):
        """Copy."""
        return self._dict.copy()
