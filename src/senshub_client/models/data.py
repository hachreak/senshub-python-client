# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Data API."""

from __future__ import absolute_import, print_function

from .resources import ProxyResource
from .boxes import ProxyBox


class ProxyData(ProxyResource):

    """ProxyData Resource."""

    @classmethod
    def get_scope(cls, userid, boxid, action=None):
        """Build the box scope.

        :param userid: User ID.
        :param boxid: Box ID.
        :param action: Action
        :returns: A string representing the user scope.
        """
        return "{0}.data".format(
            ProxyBox.get_scope(userid, boxid, action))

    @classmethod
    def read(cls, config, boxid, query=None):
        """Load data from a box.

        :param config: A
            :class:`senshub_client.models.AbstractConfiguration`
            instance.
        :param boxid: Box ID.
        :param query: String query to filter and order data.
        :returns: a class:`senshub_client.models.ProxyData` instance.
        """
        raise NotImplementedError()
