# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Boxes API."""

from __future__ import absolute_import, print_function

from .resources import ProxyResource
from .users import ProxyUser


class ProxyBox(ProxyResource):

    """Proxy Box Resource."""

    @classmethod
    def read(cls, config, boxid):
        """Load box information.

        :param config: A
            :class:`senshub_client.models.AbstractConfiguration`
            instance.
        :param boxid: The box ID.
        :returns: A :class:`senshub_client.models.ProxyBox` instance.
        """
        raise NotImplementedError()

    @classmethod
    def create(self, config, name, description=None):
        """Create a new Box.

        :param config: A
            :class:`senshub_client.models.AbstractConfiguration`
            instance.
        :param name: Box name.
        :param description: Box description.
        :returns: The new Box instance.
        """
        raise NotImplementedError()

    @classmethod
    def update(self, config, boxid, name, description=None):
        """Update box."""
        raise NotImplementedError()

    @classmethod
    def delete(cls, config, boxid):
        """Delete box."""
        raise NotImplementedError()

    @classmethod
    def get_all(cls, config):
        """Get all boxes.

        :param config: A
            :class:`senshub_client.models.AbstractConfiguration`
            instance.
        :returns: A list of class:`senshub_client.models.ProxyBox` instance.
        """
        raise NotImplementedError()

    @classmethod
    def get_scope(cls, userid, boxid, action=None):
        """Build the Box scope.

        :param userid: A user ID.
        :param boxid: A box ID.
        :param action: The scope action (one of: read, write, all).
        :returns: A string representing the box scope.
        """
        return "{0}.boxes.{1}".format(
            ProxyUser.get_scope(userid=userid, action=action), boxid)

    def push_data(self, data):
        """Save new data inside the Box.

        :param data: A dictionary containing data.
        :returns: A list of Data ID.
        """
        raise NotImplementedError()

    @classmethod
    def load_data(self, config, boxid, query=None):
        """Load data from the box.

        :param query: Specify a string query to filter and order data.
        :returns: List of Data.
        """
        raise NotImplementedError()
