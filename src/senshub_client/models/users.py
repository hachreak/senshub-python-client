# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Users API."""

from __future__ import absolute_import, print_function

from .resources import ProxyResource


class ProxyUser(ProxyResource):

    """Proxy User Resource."""

    @classmethod
    def get_scope(cls, userid, action=None):
        """Build the box scope.

        :param userid: User ID.
        :param action: Action.
        :returns: A string representing the user scope.
        """
        action = action or "all"
        return "{0}.users.{1}".format(action, userid)

    @classmethod
    def register(cls, config, **kwargs):
        """Register the user.

        :param config: Backend configuration.
        :param userid: User ID.
        :param email: User email.
        :param password: User Password.
        """
        raise NotImplementedError()

    def login(self, password, scope=None):
        """Login the user.

        :param password: The user password.
        :param scope: The scope required. If not specified, a default scope
            is built.
        :returns: The Access Token instance.
        """
        raise NotImplementedError()

    @property
    def boxes(self):
        """Get the list of boxes.

        :returns: A list of :class:`senshub_client.models.ProxyBox`.
        """
        raise NotImplementedError()

    def read(self):
        """Load the user.

        :returns: Itself.
        """
        raise NotImplementedError()

    def grant_permission(self, clientid, scope):
        """Grant some permission to the client.

        It will generate a `token_auth` that will give the permissions
        codified by the `scope`.

        After the token_auth is generated, the client will call the method
        :meth:`senshub_client.models.ProxyClient.acquire_new_token_access`
        and transform the `token_auth` in a `token_access`.

        :param clientid: The client ID.
        :param scope: A scope as `['write.users.myuser.boxes.mybox.data']`.
        :returns: The authorization token generated.
        """
        raise NotImplementedError()
