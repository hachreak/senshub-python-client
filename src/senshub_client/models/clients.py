# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Clients API."""

from __future__ import absolute_import, print_function

from .resources import ProxyResource
from .errors import WrongClientId


class ProxyClient(ProxyResource):

    """Proxy Client Resource."""

    @classmethod
    def __extract_cid(cls, string_clientid):
        """Extract userid/clientid from string_clientid.

        The FQ ClientID is like uid-<userid>-cid-<clientid>.

        :param string_clientid: The fully qualified clientid.
        :raises senshub_client.errors.WrongClientId: If the client id is
            wrongly formatted.
        :returns: A list containing [userid, clientid]
        """
        try:
            [userid, clientid] = string_clientid[4:].split("-cid-")
        except ValueError:
            raise WrongClientId()
        return [userid, clientid]

    @classmethod
    def get_scope(cls, clientid, action=None):
        """Build the box scope.

        :param clientid: Client ID.
        :param action: Action.
        :returns: A string representing the client scope.
        """
        action = action or "all"
        kwargs = cls.__extract_cid(clientid)
        return "{0}.users.{1}.clients.{2}".format(action, *kwargs)

    @classmethod
    def get_master_scope(cls, clientid):
        """Get client master scope for acquire master token.

        :param clientid: Client ID.
        :returns: The master scope.
        """
        return "{0}.tokens_access".format(cls.get_scope(clientid))

    def get_token_auths(self):
        """Get the list of Token Auths available.

        :returns: TODO
        """
        raise NotImplementedError()

    def get_token_accesses(self):
        """Get the list of Token Access available.

        :returns: TODO
        """
        raise NotImplementedError()

    def read(self, userid):
        """Load the user.

        :param userid: User ID.
        :returns: Itself.
        """
        raise NotImplementedError()

    def acquire_tokens(self, config, secret):
        """Convert new available token_auths in token_access.

        Check online if there are some token_auth available and, if there are,
        convert them in token_access.

        :param config: Configuration instance.
        :param clientid: Client ID.
        :param secret: Client secret.
        :returns: A list of token_access.
        """
        raise NotImplementedError()

    @property
    def token_accesses(self):
        """Get a list of the available token_access.

        :returns: A list of
            :class:`senshub_client.models.tokens.ProxyTokenAccess` instances.
        """
        raise NotImplementedError()

    @classmethod
    def create(self, config, clients_info):
        """Create a list of new clients.

        The information in input is in the following form:

        .. code-block:: python

            [
                {
                    "client": "<local-name>",
                    "secret": "<client-secret>"
                },
                {
                    "client": "<local-name-2>",
                    "secret": "<client-secret-2>"
                },
                ...
            ]

        In output, you have information about which was successfully created
        and the corrispondent fully qualified client id.

        :param config: A
            :class:`senshub_client.models.configurations.Configuration`
            instance.
        :param clients_info: A list of client data to use to create them.
        :returns: A list of
            :class:`senshub_client.models.tokens.ProxyClient` instances.
        """
        raise NotImplementedError()
