# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Tokens API."""

from __future__ import absolute_import, print_function

from .resources import ProxyResource


class ProxyToken(ProxyResource):

    """Generic Token used by the users or client to access to the resources."""


class ProxyTokenOwner(ProxyToken):

    """Personal token used by the user to access to his/her resources."""


class ProxyTokenAuth(ProxyToken):

    """Authorization token used by a user to grant some access to a client."""


class ProxyTokenAccess(ProxyToken):

    """Access token used by a client to access to a resource of a user."""

    def refresh(self):
        """Refresh the Access Token.

        :returns: If `True` then the user is successfully refreshed.
        """
        pass
