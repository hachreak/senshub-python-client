# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Proxy Resource."""

from __future__ import absolute_import, print_function


class ProxyResource(object):

    """A Proxy Resouce."""

    def __init__(self, model):
        """Init.

        :param model: The swagger resource model.
        """
        self.__dict__['_model'] = model

    def __getattr__(self, name):
        """Act as swagger resource proxy: get all attributes."""
        return getattr(self._model, name)

    def __setattr__(self, name, value):
        """Act as swagger resource proxy: set all attributes."""
        if self._model and hasattr(self._model, name):
            return setattr(self._model, name, value)
        else:
            self.__dict__[name] = value

    def __repr__(self):
        """Repr as swagger Box."""
        return repr(self._model)

    def to_dict(self):
        """Make the object iterable."""
        params = self._model.__dir__() if self._model else []
        return {param: getattr(self, param) for param in params}

    def __iter__(self):
        """Iterator.

        :returns: TODO
        """
        return iter(self._model.__dir__() if self._model else [])
