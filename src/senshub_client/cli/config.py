# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Config."""

from __future__ import absolute_import, print_function

import click
import os
import requests
import yaml

from .main import cli, exception_handler


def create_directory(filename):
    """Create configuration directory if doesn't exists."""
    home = os.path.dirname(filename)
    if not os.path.exists(home):
        os.makedirs(home)


def move_to_config(myfile, destination, hostname):
    """Move the file inside the configuration directory."""
    with open(destination, "w") as f:
        file_ = yaml.load(myfile)
        file_['host'] = hostname
        f.write(yaml.dump(file_))


def get_destination(filename):
    """Build destination path for the file."""
    home = '{0}/.senshub'.format(os.path.expanduser("~"))
    if not os.path.exists(home):
        os.makedirs(home)
    return '{0}/{1}'.format(home, filename)


def download_swagger(ctx, hostname, filename):
    """Download a swagger file."""
    config = ctx.obj['config']
    url = 'http://{0}/v1/docs/swagger.yaml'.format(hostname)
    destination = get_destination(filename=filename)
    click.echo('Downloading file {0} to {1}'.format(url, destination))
    myfile = requests.get(url).text
    move_to_config(myfile, destination, hostname)
    if ctx.obj['save']:
        name = 'swagger.{0}'.format(os.path.splitext(filename)[0])
        config.config[name] = 'file://{0}'.format(destination)
        config.save()


@cli.group()
def config():
    """Configure the application."""
    pass


@config.group()
def download():
    """Download configuration files."""
    pass


@download.command()
@click.argument('hostname')
@click.pass_context
@exception_handler
def all(ctx, hostname):
    """Download all swagger configuration files in one step."""
    download_swagger(
        ctx=ctx, hostname='{0}:8080'.format(hostname), filename='api.yaml')
    download_swagger(ctx=ctx, hostname='{0}:8081'.format(hostname),
                        filename='oauth2.yaml')


@download.command()
@click.argument('hostname')
@click.pass_context
@exception_handler
def api(ctx, hostname):
    """Download swagger files for API."""
    download_swagger(
        ctx=ctx, hostname='{0}:8080'.format(hostname), filename='api.yaml')


@download.command()
@click.argument('hostname')
@click.pass_context
@exception_handler
def oauth2(ctx, hostname):
    """Download swagger files for OAuth2."""
    download_swagger(ctx=ctx, hostname='{0}:8081'.format(hostname),
                        filename='oauth2.yaml')
