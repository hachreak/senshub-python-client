# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Init."""

from __future__ import absolute_import, print_function

import click
import collections
import simplejson

from bravado.exception import HTTPError
from functools import wraps

from ..http_rest import Configuration
from ..models.errors import CorruptedConfigurationFile, TokenError, \
    WrongConfiguration


@click.group()
@click.option('--config', default="senscli.json")
@click.option('--save/--no-save', default=False,
              help="Save on disk the new acquired information.")
@click.pass_context
def cli(ctx, config, save):
    """Senshub CLI."""
    try:
        ctx.obj = {
            'config': Configuration(filename=config)
        }
        ctx.obj['save'] = save
    except IOError as e:
        raise click.UsageError(e)
    except CorruptedConfigurationFile as e:
        raise click.UsageError("Configuration file {} is corrupted".format(
            e.filename))


def exception_handler(f):
    """Default HTTP exceptions handler."""
    @wraps(f)
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except (HTTPError, TokenError) as e:
            raise click.UsageError(e)
        except WrongConfiguration as e:
            raise click.UsageError("Configuration variable {} is wrong or not "
                                   "found".format(e.key))
    return wrapped


def validate_json(ctx, param, source):
    """Validate json in input.

    :param input: TODO
    :returns: TODO
    """
    try:
        json_file = simplejson.load(source)
    except simplejson.scanner.JSONDecodeError:
        raise click.UsageError("Wrong clients input. It must be a valid json.")

    if not isinstance(json_file, collections.MutableSequence):
        json_file = [json_file]

    return json_file


def default_options(ctx, param, options):
    """Get option from input and update loaded configuration file.

    :param ctx: TODO
    :param param: TODO
    :param value: TODO
    :returns: TODO
    """
    for option in options:
        [key, value] = option.split('=', 1)
        ctx.obj['config'].config[key] = value
