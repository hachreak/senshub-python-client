# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Clients CLI."""

from __future__ import absolute_import, print_function

import click

from .main import cli, exception_handler, default_options
from ..http_rest import Client


@cli.group()
def clients():
    """The Clients CLI."""
    pass


@clients.command()
@click.argument('clientid')
@click.pass_context
@exception_handler
def read(ctx, clientid):
    """Load a client.

    :param ctx: TODO
    :param clientid: TODO
    """
    #  stderr = click.get_text_stream('stderr')
    #  stderr.write("fuu")
    pass


@clients.group()
def tokens():
    """Token sub-commands."""
    pass


@tokens.command()
@click.pass_context
@exception_handler
def refresh(ctx):
    """Refresh a token_access.

    :param ctx: TODO
    """
    pass


@tokens.command()
@click.pass_context
@exception_handler
def list(ctx):
    """Get a list of available token_access.

    :param ctx: TODO
    """
    pass


@tokens.command()
@click.argument('clientid')
@click.option('--options', multiple=True, callback=default_options)
@click.option('--secret', prompt=True, hide_input=True)
@click.pass_context
@exception_handler
def acquire(ctx, clientid, secret, **kwargs):
    """Acquire new token_access."""
    config = ctx.obj['config']
    config.config['client._id'] = clientid
    config.config['client.secret'] = secret
    tokens = Client(config=config).acquire_tokens(config=config, secret=secret)
    if ctx.obj['save']:
        for token in tokens:
            config.tokens[token.token_access] = token
        config.save()
    for token in tokens:
        click.echo(token.to_dict())
