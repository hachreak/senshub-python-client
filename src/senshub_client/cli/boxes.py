# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Boxes CLI."""

from __future__ import absolute_import, print_function

import click
from tabulate import tabulate

from .main import cli, exception_handler
from ..http_rest import Box

def extract_info(box):
    """Extract printable info about the box.

    :param box: TODO
    :returns: TODO
    """
    # FIXME API v1.1: return from API the _bid as _id
    return [box._id._bid if hasattr(box._id, '_bid') else box._id,
            box.name, box.description or '']


#  def did(data):
#      data._model['_id'] = data._model['_id']['_did']
#      return data

#  def data_extract_info(data):
#      """Extract printable info about a list of data.

#      :param data: TODO
#      :returns: TODO
#      """
#      return map(lambda d: did(d)._model, data)


@cli.group()
def boxes():
    """The Boxes CLI."""
    pass


@boxes.command()
@click.pass_context
@exception_handler
def list(ctx):
    """List user boxes."""
    config=ctx.obj['config']
    table = [extract_info(box) for box in Box.get_all(config=config)]
    click.echo(tabulate(table, tablefmt="plain"))


@boxes.command()
@click.argument('name')
@click.option('-d', '--description')
@click.pass_context
@exception_handler
def create(ctx, **kwargs):
    """Create a new box.

    :param ctx: The context
    :param name: The box name.
    :param description: The box description.
    """
    config = ctx.obj['config']
    boxid = Box.create(config=config, **kwargs)
    print(boxid)


@boxes.command()
@click.argument('boxid')
@click.pass_context
@exception_handler
def read(ctx, boxid):
    """Load a box information.

    :param ctx: The context
    :param boxid: The box ID.
    """
    config = ctx.obj['config']
    click.echo(tabulate([
        extract_info(Box.read(config=config, boxid=boxid))
    ], tablefmt="plain"))


@boxes.command()
@click.argument('boxid')
@click.argument('name')
@click.option('-d', '--description')
@click.pass_context
@exception_handler
def update(ctx, boxid, **kwargs):
    """Update a new box.

    :param ctx: The context
    :param boxid: The box ID.
    :param name: The box name.
    :param description: The box description.
    """
    config = ctx.obj['config']
    Box.update(config=config, boxid=boxid, **kwargs)


@boxes.command()
@click.argument('boxid')
@click.option('-q', '--query')
@click.pass_context
@exception_handler
def query(ctx, boxid, query):
    """Query a box.

    :param ctx: The context
    :param boxid: The box ID.
    :param query: The query used to filter and order the data.
    """
    config = ctx.obj['config']
    data = Box.load_data(config=config, boxid=boxid, query=query)
    click.echo(data)


@boxes.command()
@click.argument('boxid')
@click.pass_context
@exception_handler
def delete(ctx, boxid):
    """Delete a box.

    :param ctx: The context
    :param boxid: The box ID.
    """
    config = ctx.obj['config']
    Box.delete(config=config, boxid=boxid)
