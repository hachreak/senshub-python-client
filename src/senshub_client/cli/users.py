# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Users CLI."""

from __future__ import absolute_import, print_function

import click
import sys

from .main import cli, exception_handler, validate_json
from ..http_rest import User, Client


def validate_scope(ctx, param, value):
    """Validate scope."""
    # TODO
    return list(value)


@cli.group()
def users():
    """The Users CLI."""
    pass


@users.command()
@click.argument('userid')
@click.argument('email')
@click.password_option()
@click.pass_context
@exception_handler
def register(ctx, **kwargs):
    """Register a new user."""
    User.register(config=ctx.obj['config'], **kwargs)


@users.command()
@click.argument('userid')
@click.pass_context
@exception_handler
def read(ctx, userid):
    """Load a user."""
    config = ctx.obj['config']
    config.config['user._id'] = userid
    user = User(config=config).read().to_dict()
    if ctx.obj['save']:
        config.config['user'].update(user)
        config.save()
    click.echo(user)


@users.command()
@click.argument('userid')
@click.option('--password', prompt=True, hide_input=True)
@click.argument('scope', nargs=-1)
@click.pass_context
@exception_handler
def login(ctx, userid, password, scope):
    """Login the user and return the token_access."""
    config = ctx.obj['config']
    config.config['user._id'] = userid
    config.config['user.password'] = password
    config.config['user.scope'] = scope
    token = User(config=config).login(password=password, scope=list(scope))
    if ctx.obj['save']:
        config.tokens[token.token_access] = token
        config.save()
    click.echo(token.to_dict())


@users.group()
def clients():
    """User Clients CLI."""
    pass


@clients.command()
@click.argument('source', type=click.File('r'), default=sys.stdin,
                callback=validate_json)
@click.pass_context
@exception_handler
def create(ctx, source):
    """Create a list of clients."""
    clients = Client.create(config=ctx.obj['config'], clients_info=source)
    for client in clients:
        click.echo(client._id)


@clients.group()
def permissions():
    """Manage client permissions CLI."""
    pass


@permissions.command()
@click.argument('clientid')
@click.argument('scope', nargs=-1, required=True, callback=validate_scope)
@click.pass_context
@exception_handler
def grant(ctx, clientid, scope):
    """Grant permissions to a client through a token_auth release."""
    config = ctx.obj['config']
    user = User(config=config)
    token = user.grant_permission(clientid=clientid, scope=scope)
    click.echo(token.to_dict())


@permissions.command()
@click.argument('clientid')
@click.pass_context
@exception_handler
def master(ctx, clientid, **kwargs):
    """Grant master token for a client."""
    config = ctx.obj['config']
    user = User(config=config)
    token = user.grant_permission(
        clientid=clientid, scope=Client.get_master_scope(clientid))
    click.echo(token.to_dict())
