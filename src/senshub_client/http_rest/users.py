# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""HTTP REST Backend for users."""

from __future__ import absolute_import, print_function

from ..models import ProxyUser, ProxyTokenOwner, ProxyTokenAuth
from .boxes import Box
from .clients import Client


class User(ProxyUser):

    """HTTP REST User backend."""

    def __init__(self, config, model=None):
        """A User implementation using the REST API.

        :param config: Backend configuration
        :param model: The swagger User model.
        """
        if not model:
            client = config.client_api
            User = client.get_model('User')
            model = User(**config.config['user'])
        ProxyUser.__init__(self, model=model)
        self._config = config

    @classmethod
    def register(cls, config, **kwargs):
        """Register.

        See: :meth:`senshub_client.models.ProxyUser.register`.
        """
        client = config.client_api
        UserCreate = client.get_model('UserCreate')
        client.User.controllers_users_create(
            user=UserCreate(**kwargs)).result()

    def login(self, password, scope=None):
        """Login the user.

        See: :meth:`senshub_client.models.ProxyUser.login`.
        """
        if not scope:
            scope = [User.get_scope(self._id)]
        client = self._config.client_oauth2
        UserLogin = client.get_model('UserLogin')
        user_login = UserLogin(password=password, scope=scope)
        return ProxyTokenOwner(
            model=client.User.get_token_owner(
                userid=self._id, user_login=user_login).result()
        )

    def read(self):
        """Load a user.

        See: :meth:`senshub_client.models.ProxyUser.load`.
        """
        client = self._config.client_api
        userid = self._config.config._dict['user']['_id']
        user_model, _ = client.User.controllers_users_get_by_id(
            userid=userid).result()
        self._model = user_model
        return self

    @property
    def boxes(self):
        """Get the list of boxes.

        See: :meth:`senshub_client.models.ProxyUser.boxes`.
        """
        return Box.get_all(config=self._config)

    def grant_permission(self, clientid, scope):
        """Grant some permission to the client.

        See: :meth:`senshub_client.models.ProxyUser.grant_permission`.
        """
        if not isinstance(scope, list):
            scope = [scope]
        client = self._config.client_oauth2
        AuthCodeRequest = client.get_model('AuthCodeRequest')
        auth_code_request = AuthCodeRequest(
            scope=scope, userid=self._id,
            password=self.password)
        token_auth = client.User.post_token_auth(
            clientid=clientid, auth_code_request=auth_code_request).result()
        return ProxyTokenAuth(model=token_auth)

    def create_clients(self, clients_info):
        """Create clients.

        :param clients_info: A list of information for each client.
        :returns: A list of :class:`senshub_client.models.s.ProxyClient`.
        """
        return Client.create(config=self._config, clients_info=clients_info)
