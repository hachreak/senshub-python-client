# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""HTTP REST Backend for data."""

from __future__ import absolute_import, print_function

from ..models import ProxyData
from .utils import build_headers


class Data(ProxyData):

    """HTTP REST Data backend."""

    def __init__(self, config, **kwargs):
        """A Data implementation using the REST API.

        :param config: Backend configuration
        :param **kwargs: The :class:`senshub_client.models.ProxyData`
            configuration.
        """
        ProxyData.__init__(self, **kwargs)
        self._config = config

    @classmethod
    def read(cls, config, boxid, query=None):
        """Load data from a box.

        See: :meth:`senshub_client.models.ProxyData.read`.
        """
        client = config.client_api
        result, _ = client.Data.controllers_data_get_all(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            boxid=boxid,
            q=query
        ).result()
        return [
            Data(config=config, model=data_model) for data_model in result
        ]
