# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""HTTP REST Backend for boxes."""

from __future__ import absolute_import, print_function

from ..models import ProxyBox
from ..models.utils import adapt_data
from .data import Data
from .utils import build_headers


class Box(ProxyBox):

    """HTTP REST Box backend."""

    def __init__(self, config, model=None, **kwargs):
        """A Box implementation using the REST API.

        :param config: Backend configuration
        :param **kwargs: The :class:`senshub_client.models.ProxyBox`
            configuration.
        """
        if not model:
            client = config.client_api
            model = client.get_model('Box')
        ProxyBox.__init__(self, model=model)
        for (k, v) in kwargs.items():
            setattr(self, k, v)
        self._config = config

    @classmethod
    def create(cls, config, **kwargs):
        """Create a new Box.

        See: :meth:`senshub_client.models.ProxyBox.create`.
        """
        client = config.client_api
        BoxCreate = client.get_model('BoxCreate')
        _, response = client.Box.controllers_boxes_create(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            box=BoxCreate(**kwargs)
        ).result()
        return response.headers['location'].split("/")[-1]

    @classmethod
    def update(cls, config, boxid, **kwargs):
        """Update box.

        See: :meth:`senshub_client.models.ProxyBox.update`.
        """
        client = config.client_api
        BoxCreate = client.get_model('BoxCreate')
        box_create = BoxCreate(**kwargs)
        client.Box.controllers_boxes_update(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            boxid=boxid,
            box=box_create
        ).result()

    @classmethod
    def delete(cls, config, boxid):
        """Delete box.

        See: :meth:`senshub_client.models.ProxyBox.delete`.
        """
        client = config.client_api
        client.Box.controllers_boxes_delete(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            boxid=boxid
        ).result()

    def push_data(self, data):
        """Save new data inside the Box.

        See: :meth:`senshub_client.models.ProxyBox.push_data`.
        """
        client = self._config.client_api
        client.Data.controllers_boxes_push(
            _request_options={
                "headers": build_headers(self._config.tokens)
            },
            boxid=self._id,
            data=adapt_data(data)
        ).result()

    @classmethod
    def load_data(cls, config, boxid, query=None):
        """Load data from the box.

        See: :meth:`senshub_client.models.ProxyBox.load_data`.
        """
        return Data.read(config=config, boxid=boxid, query=query)

    @classmethod
    def get_all(cls, config):
        """Get all boxes.

        See: :meth:`senshub_client.models.ProxyBox.get_all`.
        """
        client = config.client_api
        result, _ = client.Box.controllers_boxes_get_all(
            _request_options={
                "headers": build_headers(config.tokens)
            }
        ).result()
        return [Box(config=config, model=box_model) for box_model in result]

    @classmethod
    def read(cls, config, boxid):
        """Load box.

        See: classmethod:`senshub_client.models.ProxyBox:read`.
        """
        client = config.client_api
        box_model, _ = client.Box.controllers_boxes_get(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            boxid=boxid
        ).result()
        return Box(config=config, model=box_model)
