# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Configuration loader."""

from __future__ import absolute_import, print_function

import simplejson
import os

from simplejson.scanner import JSONDecodeError
from bravado.client import SwaggerClient

from ..models import AbstractConfiguration, ModelJSONEncoder, \
    ProxyTokenOwner, ProxyTokenAccess
from ..models.errors import CorruptedConfigurationFile
from ..models.utils import DotDict

# TODO AbstractConfiguration -> dict


class Configuration(AbstractConfiguration):

    """HTTP REST Backend Configuration."""

    def __init__(self, filename):
        """Init configuration.

        :param filename: Configuration file name. E.g. `myconf.json`.
        """
        self.filename = filename
        #  self.config = DotDict({})
        self._load()

    def _load(self):
        """Load configuration."""
        #  if not self.config:
        # if not exists, create empty file
        if not os.path.exists(self.filename):
            with open(self.filename, "w") as f:
                f.write('{}')

        # load from json file
        try:
            with open(self.filename) as config_file:
                self.config = DotDict(simplejson.load(config_file))
        except JSONDecodeError:
            raise CorruptedConfigurationFile(self.filename)

        # set default tokens
        if 'tokens' not in self.config:
            self.config['tokens'] = {}

    def _load_tokens(self):
        # convert token in objects
        client = self.client_oauth2
        AccessTokenResponse = client.get_model('AccessTokenResponse')
        OwnerTokenResponse = client.get_model('OwnerTokenResponse')
        for k, v in self.config.get('tokens', {}).items():
            if 'token_refresh' in v:
                # load access token
                self.config['tokens'][k] = ProxyTokenAccess(
                    model=AccessTokenResponse(**v))
            else:
                # load owner token
                self.config['tokens'][k] = ProxyTokenOwner(
                    model=OwnerTokenResponse(**v))

    @property
    def tokens(self):
        """Get the configured dictionary of token_access.

        :returns: A dictionary of token_access
        """
        self._load_tokens()
        return self.config['tokens']

    @property
    def client_api(self):
        """Return a Swagger client for API.

        :returns: A class:`bravado.client:SwaggerClient` client.
        """
        if not hasattr(self, '_client_api'):
            self._client_api = SwaggerClient.from_url(
                self.config.get('swagger', {}).get('api'),
                config={'also_return_response': True}
            )
        return self._client_api

    @client_api.setter
    def client_api(self, name):
        """Set swagger API filename."""
        #  self._load()
        self.config['swagger.api'] = name

    @property
    def client_oauth2(self):
        """Return a Swagger client for OAuth2.

        :returns: A class:`bravado.client:SwaggerClient` client.
        """
        if not hasattr(self, '_client_oauth2'):
            self._client_oauth2 = SwaggerClient.from_url(
                self.config.get('swagger', {}).get('oauth2'))
        return self._client_oauth2

    @client_oauth2.setter
    def client_oauth2(self, name):
        """Set swagger OAuth2 filename."""
        #  self._load()
        self.config['swagger.oauth2'] = name

    def save(self):
        """Save configuration on disk."""
        with open(self.filename, 'w') as config_file:
            config_file.write(
                # TODO how can we use self.config?
                simplejson.dumps(self.config._dict, sort_keys=True, indent=4,
                                 cls=ModelJSONEncoder)
            )
