# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""HTTP REST Backend for clients."""

from __future__ import absolute_import, print_function

from ..models import ProxyClient, ProxyTokenAccess
from .utils import build_headers


class Client(ProxyClient):

    """HTTP REST Client backend."""

    def __init__(self, config, model=None, **kwargs):
        """A Client implementation using the REST API.

        :param config: Backend configuration
        :param model: The swagger Client model.
        :param **kwargs: A :class:`senshub_client.models.Client`
            configuration.
        """
        if not model:
            client = config.client_api
            Client = client.get_model('Client')
            model = Client(**config.config.get('client', {}))
        ProxyClient.__init__(self, model=model)
        self._config = config

    def read(self, clientid):
        """Load the client.

        See: :meth:`senshub_client.models.ProxyClient.load`.
        """
        # FIXME
        client = self._config.client_api
        user_model, _ = client.Client.controllers_users_get_by_id(
            clientid=clientid).result()
        self._model = user_model
        return self

    def acquire_tokens(self, config, secret):
        """Convert new available token_auths in token_access.

        See:
        :meth:`senshub_client.models.ProxyClient.acquire_new_token_accesses`.
        """
        client = config.client_oauth2
        # Get token_auth list
        auths = client.Client.get_token_auths(
            clientid=self._id, secret=secret).result()
        tokens = []
        # for each token_auth convert it in a token_access
        AccessTokenRequest = client.get_model('AccessTokenRequest')
        for auth in auths:
            request = AccessTokenRequest(
                secret=secret, token_auth=auth.token_auth)
            tokens.append(
                ProxyTokenAccess(model=client.Client.post_token_access(
                    clientid=self._id, access_token_request=request
                ).result())
            )
        return tokens

    @property
    def token_accesses(self):
        """Get a list of the available token_access.

        See: :meth:`senshub_client.models.ProxyClient.token_accesses`.
        """
        raise NotImplementedError()

    @classmethod
    def create(cls, config, clients_info):
        """Create a list of new clients.

        See: :meth:`senshub_client.models.create`.
        """
        client = config.client_api
        # Create
        ClientCreate = client.get_model('ClientCreate')
        clients = [
            ClientCreate(**cinfo) for cinfo in clients_info
        ]
        results, _ = client.Client.controllers_clients_bulk_create(
            _request_options={
                "headers": build_headers(config.tokens)
            },
            clients=clients
        ).result()
        SwaggerClient = client.get_model('Client')
        # Return the list as list of Client
        return [
            Client(
                config=config,
                model=SwaggerClient(_id=result.clientid)
            ) for result in results if result.status == 'ok'
        ]
