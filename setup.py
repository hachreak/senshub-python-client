# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Setup."""

from __future__ import absolute_import, print_function

import io
import re
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    """Read."""
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()


setup(
    name='senshub_client',
    version='0.1.0',
    license='LGPL v2.1',
    description='senshub_client is a client library for the senshub backend.',
    long_description='%s\n%s' % (read('README.rst'),
                                 re.sub(':[a-z]+:`~?(.*?)`', r'``\1``',
                                        read('CHANGELOG.rst'))),
    author='Leonardo Rossi',
    author_email='leonardo.rossi@studenti.unipr.it',
    url='https://github.com/hachreak/senshub_client',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list:
        # http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Utilities',
    ],
    keywords=[
        # eg: 'keyword1', 'keyword2', 'keyword3',
    ],
    install_requires=[
        'bravado>=8.1.2',
        # FIXME modify e.g. client.Box.controllers_boxes_get_all(api_key="123")
        'bravado-core==4.3.2',
        'click>=6.6',
        'requests>=2.11',
        'simplejson>=3.10',
        'tabulate>=0.7.5',
    ],
    extras_require={
        'test': [
            'pytest>=2.8.0',
        ]
        # eg:
        #   'rst': ['docutils>=0.11'],
        #   ':python_version=="2.6"': ['argparse'],
    },
    entry_points={
        'console_scripts': [
            'senscli = senshub_client.cli:cli',
        ]
    },
)
