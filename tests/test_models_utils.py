# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Test HTTP REST Configuration."""

from __future__ import absolute_import, print_function

import pytest

from copy import deepcopy
from datetime import datetime
from senshub_client.models.utils import adapt_data, DotDict


def test_adapt_data():
    """Test adapt_data."""
    assert 1 == adapt_data(1)
    assert "hello" == adapt_data("hello")
    assert "2014-10-04T13:00:02Z" == adapt_data(
        datetime(2014, 10, 4, 13, 0, 2, 749890))
    assert [{
        "temperature": 12,
        "when": "2014-10-04T13:00:02Z",
        "name": "john",
        "where": {
            "latitude": 40.9418601,
            "longitude": 3.585612,
        }
    }] == adapt_data([{
        "temperature": 12,
        "when": datetime(2014, 10, 4, 13, 0, 2, 749890),
        "name": "john",
        "where": {
            "latitude": 40.9418601,
            "longitude": 3.585612,
        }
    }])


def test_dotdict_get():
    """Test DotDict get."""
    fuu = DotDict({
        'a': {
            'b': 'c'
        },
        'd': 'e'
    })

    assert 'c' == fuu['a.b']
    assert 'g' == fuu.get('f', 'g')
    assert 'g' == fuu.get('a.f', 'g')
    with pytest.raises(KeyError):
        fuu['not.exist']


def test_dotdict_set():
    """Test DotDict set."""
    mydict = {
        'a': {
            'b': 'c'
        },
        'd': 'e'
    }
    mydict_copy = deepcopy(mydict)

    fuu = DotDict(mydict)

    fuu['a.f'] = 'g'
    mydict_copy['a']['f'] = 'g'

    assert fuu._dict == mydict_copy
