# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Test Clients API."""

from __future__ import absolute_import, print_function

import pytest
from senshub_client.models import ProxyClient
from senshub_client.models.errors import WrongClientId


def test_scope():
    """Test scope."""
    scope = ProxyClient.get_scope(
        clientid="uid-pippo-cid-ba7260b6-eea0-415c-83f5-43fa46729259",
        action="write")
    assert "write.users.pippo.clients.ba7260b6-eea0-415c-83f5-43fa46729259" \
        == scope
    scope = ProxyClient.get_scope(
        clientid="uid-pippo-cid-ba7260b6-eea0-415c-83f5-43fa46729259",
        action="read")
    assert "read.users.pippo.clients.ba7260b6-eea0-415c-83f5-43fa46729259" \
        == scope
    scope = ProxyClient.get_scope(
        clientid="uid-pippo-cid-ba7260b6-eea0-415c-83f5-43fa46729259",
        action="all")
    assert "all.users.pippo.clients.ba7260b6-eea0-415c-83f5-43fa46729259" \
        == scope
    scope = ProxyClient.get_scope(
        clientid="uid-pippo-cid-ba7260b6-eea0-415c-83f5-43fa46729259")
    assert "all.users.pippo.clients.ba7260b6-eea0-415c-83f5-43fa46729259" \
        == scope
    with pytest.raises(WrongClientId):
        ProxyClient.get_scope(
            clientid="uid-pippo-cid-ba7260b6-cid-eea0-415c-83f5-43fa46729259")
