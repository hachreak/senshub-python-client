# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Test Users API."""

from __future__ import absolute_import, print_function

from senshub_client.models import ProxyUser


def test_scope():
    """Test scope."""
    scope = ProxyUser.get_scope(userid="pippo", action="write")
    assert "write.users.pippo" == scope
    scope = ProxyUser.get_scope(userid="pippo", action="read")
    assert "read.users.pippo" == scope
    scope = ProxyUser.get_scope(userid="pippo", action="all")
    assert "all.users.pippo" == scope
    scope = ProxyUser.get_scope(userid="pippo")
    assert "all.users.pippo" == scope
