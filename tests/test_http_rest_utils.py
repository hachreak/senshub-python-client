# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Test HTTP REST Configuration."""

from __future__ import absolute_import, print_function

import pytest

from collections import namedtuple
from senshub_client.http_rest.utils import build_headers
from senshub_client.models.errors import TokenError


def test_build_headers():
    """Test scope."""
    TokenAccess = namedtuple('TokenAccess', 'token_access')
    value = '123'
    token = TokenAccess(token_access=value)
    tokens = {value: token}
    headers = build_headers(tokens=tokens)
    expected = {
        "Authorization": "Basic 123",
        "Content-type": "application/json",
    }
    assert headers == expected
    assert {value: token} == tokens


def test_build_headers_no_token():
    """Test build headers without token."""
    with pytest.raises(TokenError):
        build_headers(tokens=[])
    with pytest.raises(TokenError):
        build_headers(tokens={})
    with pytest.raises(TokenError):
        build_headers(tokens={'123': '123'})
