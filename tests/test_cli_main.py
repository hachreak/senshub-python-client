# -*- encoding: utf-8 -*-
#
# This file is part of senshub-python-client.
# Copyright 2016 Leonardo Rossi <leonardo.rossi@studenti.unipr.it>.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

"""Test Users API."""

from __future__ import absolute_import, print_function, unicode_literals

import click
import pytest

from io import StringIO
from bravado.exception import HTTPError

from senshub_client.cli.main import exception_handler, validate_json


def test_validate_json():
    """Test validate_json."""
    # check unvalid
    unvalid_json = StringIO("""unvalid json""")
    with pytest.raises(click.UsageError):
        validate_json(None, None, unvalid_json)

    # check is list
    no_list = StringIO("""{"name": "fuu"}""")
    assert [{"name": "fuu"}] == validate_json(None, None, no_list)


def test_exception_handler():
    """Test exception_handler."""
    @exception_handler
    def fun():
        raise HTTPError(409)

    with pytest.raises(click.UsageError):
        fun()
